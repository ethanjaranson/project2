/*-------------------------------------------------------------------------------------
A standard tree implementation using a linked list implementation.
--------------------------------------------------------------------------------------*/

#include "Tree.h"
#include <iostream>

//***********************************Public Functions*********************************
/*------------------------------------------------------------------------------------
The default tree constructor.

Preconditions: none.
Postconditions: treeSize = 0;
-------------------------------------------------------------------------------------*/

Tree::Tree(){
	treeSize = 0;
	root = NULL;
}

/*-----------------------------------------------------------------------------------
the default tree destructor.
Preconditions: none;
Postconditions: treesize = 0; parent, leftchild, rightchild = NULL;
-----------------------------------------------------------------------------------*/
	Tree::~Tree(){
		treenode * temp;
		while (root != NULL){
			temp = Minimum(root);
			delete temp;
		}
	}

/*----------------------------------------------------------------------------------
Push inserts a new value into the tree

Preconditions: none.
Postconditions: treeSize > 0, Parent could = NULL IF TREESIZE = 0; Parent != NULL if
treesize > 0. Leftchild = NULL; rightchild = NULL;
----------------------------------------------------------------------------------*/

void Tree::addValue(string value, treenode *start){
	treenode *temp = new treenode;
	temp -> value = value;
	temp -> alive = true;
	root = start;
	if(root == NULL){
		temp->parent = root;
		temp->leftchild = root;
		temp->rightchild = root;
		root = temp;
		treeSize++;
	}
	if(root != NULL){
		if(value > root -> value && root -> rightchild == NULL){
			root -> rightchild -> value = value;
			temp -> parent = root;
			root -> rightchild = temp;
			treeSize++;
		}
		else{
			addValue(value, root->rightchild);
			root -> rightchild = temp;
		}
		if(value < root->value  && root -> leftchild == NULL){
			root -> leftchild -> value = value;
			temp -> parent = root;
			root ->leftchild = temp;
			treeSize++;
		}
		else{
			addValue(value, root->leftchild);
			root -> leftchild = temp;
		}
	}		
}


bool Tree::removeValue(string value, treenode *start){
	treenode* temp = Search(value, start);
	if (temp == NULL){
		cout << "Value not in tree";
		return false;
	}
	else{
		temp -> alive = false;
		cout << "Value has been turned off or is removed";
		return true;
	}
}

treenode * Tree::returnRoot() const{
	return root;
}

void Tree::Print(){
	treenode *temp = Minimum(root); //gets the lowest value in the tree
	while (root -> alive =! false){  //prints until root is reached
		if (temp -> alive =! false){  // only prints if it is alive
			cout << temp -> value;
			temp = Predecessor(temp);
		}
		else{						// if the node is dead gets next highest value
			temp = Predecessor(temp); // sets next value to be looked at.
		}
		if (temp == NULL){  //checks to see if root is equal to null;
			root -> alive = false; // if it is we are taken out loop;
		}

	}
	root -> alive = true; //maybe use a parent instead of root
}

int Tree::NumOfTreeNodes() const{
	return treeSize;
}

//Written by MikeyG
treenode * Tree::Search (string value, treenode *start){
	if (start == NULL){
		return NULL;
	}
	if (start -> value == value){
		return start;
	}
	if (value < start -> value){
		return (Search(value, start->leftchild));
	}
	else{
		return (search(value, start ->rightchild));
	}
}

//Written by MikeyG
treenode * Tree::Minimum(treenode *start){
	if(start == NULL){
		return NULL;
	}
	while(start ->leftchild != NULL){
		start = start -> leftchild;
	}
	return start;
}

//Not provided by MikeyG but using essentially same code as maximum which was
//provided by MikeyG
treenode * Tree::Maximum(treenode *start) const{
	if (start == NULL){
		return NULL;
	}
	while(start->rightchild != NULL){
		start = start ->rightchild;
	}
	return start;
}

//Not provided by MikeyG but using essentially same code as successor which was
//provided by MikeyG
treenode * Tree::Predecessor(treenode *here){
	//easy case
	if (here-> leftchild != NULL){
		return Maximum(here->leftchild);
	}
	//harder case
	while(parent != NULL && here = parent ->leftchild){
		here = parent;
		parent = here -> parent;
	}
	return parent;
}

//Written by MikeyG
treenode * Tree::Successor(treenode *here) const{
	//easy case
	if (here-> rightchild != NULL){
		return Minimum(here->rightchild);
	}
	//harder case
	parent = here->parent;
	while(parent != NULL && here = parent ->rightchild){
		here = parent;
		parent = here -> parent;
	}
	return parent;
}


