#ifndef _TREENODE_H
#define _TREENODE_H

/*--------------------------------------------------------------------------------------
A standard linked list implementation of the Tree Class. Given the linked list
implementation, there is no upper limit on the number of elements the tree can
contain
--------------------------------------------------------------------------------------*/

#include <iostream>

using namespace std;

//Definition of a tree node.

struct treenode {
	string value;
	bool alive;
	treenode * leftchild;
	treenode * rightchild;
	treenode * parent;
};

class Tree {
	
	public:
	//Constructors and Destructors
	/*----------------------------------------------------------------------------------
	The default tree constructor provides the caller initially with an empty tree. 
	----------------------------------------------------------------------------------*/

	Tree();

	/*----------------------------------------------------------------------------------
	The default tree destructor: deallocates all dynamically allocated memory (i.e. treenodes)
	----------------------------------------------------------------------------------*/

	~Tree();


	//Modification Methods
	/*----------------------------------------------------------------------------------
	The addValue methods adds the provided value (value) to the tree.
	Precondition: If tree is empty the added value is the root.
	Postcondition: The number of elements being stored on the tree is one greater than
	before. (IsEmpty == false).
	-----------------------------------------------------------------------------------*/

	void addValue(string value, treenode *start);

	/*----------------------------------------------------------------------------------
	The removeValue method removes the value (value) from the tree if the value is in 
	the tree. 
	Precondition: The value must be in the tree. (treenode *temp -> alive == true).
	Postcondition: (treenode *temp -> alive == false).
	----------------------------------------------------------------------------------*/

	bool removeValue(string value, treenode *start);

	//Constant Methods
	/*----------------------------------------------------------------------------------
	The returnRoot methods returns the root of the tree. It returns to the caller the 
	value (via a reference parameter). Returned with a pointer.
	----------------------------------------------------------------------------------*/

	treenode* returnRoot() const;

	

	/*----------------------------------------------------------------------------------
	The Print method returns the elements in the tree in a in order traversal. Starting
	with the lowest values and ascending.
	---------------------------------------------------------------------------------*/

	void Print();

	/*----------------------------------------------------------------------------------
	The NumberOfTreeNodes method returns the number of elements in the tree.
	----------------------------------------------------------------------------------*/

	int NumOfTreeNodes() const;

	/*----------------------------------------------------------------------------------
	The Search method returns to the user a pointer to the value requested
	from the tree.
    The value is returned via a pointer. 
	----------------------------------------------------------------------------------*/

	treenode *Search (string value, treenode *start);

	/*-------------------------------------------------------------------------------
	The Minimum method returns to the user the lowest value in the tree. Returned
	via a pointer.
	-------------------------------------------------------------------------------*/

	treenode *Minimum(treenode *start);

	/*-------------------------------------------------------------------------------
	The Maximum method returns to the user the largest value in the tree. Returned
	via a pointer.
	------------------------------------------------------------------------------*/

	treenode *Maximum(treenode *start) const;

	/*-----------------------------------------------------------------------------
	The Predecessor method returns to the user the previous value in a subtree.
	------------------------------------------------------------------------------*/

	treenode *Predecessor(treenode *here);

	/*-----------------------------------------------------------------------------
	The Successor method returns to the user the previous value in a subtree of 
	a specified node.
	-----------------------------------------------------------------------------*/

	treenode *Successor(treenode *here);


private:
	//Object instance data
	treenode *root;
	int treeSize;

};

#endif